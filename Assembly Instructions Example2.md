# PROJECT REPORT
**TITLE:  MINI ROCKET FROM ALUMINIUM FOIL**



****TEAM MEMBERS****
- Josna Sebastian
- Somya Jain
- Venkata Sampat Krishna Bharadwaj Komanduri
- Almy Ruzni

**SAFETY PRECAUTIONS**
- Do not try this inside house.
- Maintain a safe distace with it is fired.
- Wear safety goggles.


**MATERIALS:**
* Matchsticks 
* Foil (l * b - 6 * 5)
* Tape
* Pliers
* Chopstick 

<img src="images/materials.png" width="400" height="400" style="transform:rotate(90deg)">

**METHODOLOGY:**
1. Cut the foil into a rectangle shape in the dimension 6*5 cm. 
2. Remove the head of seven matchsticks and ground them  into fine powder. 
3. Using a chopstick, roll the foil diagonaly and close one end with a plier. 
4. Grounded matchstick powder should be then filled into the rolled foil. 
5. Insert the chopstick into the open end of the foil. 
6. The whole system is then placed at 45 degree to 60 degree with the support of a paper and a tape.

<img src="images/Final_assembly.png" width="400" height="400"/>

8. Then the system can be lighted using a lighter.

**FORCES ACTING ON THE ROCKET**
* Weight
* Thrust
* Lift and drag.


**RESULT:**
1. Newton's third law is applied, every action has an equal and opposite reaction. 
2. The rocket propelled from the position to a distance of approx. 2 meters. 


**SUMMARY**
|                                                                                      | Safety | Learning | Fun Level | Difficulty | Stree Level |
|:------------------------------------------------------------------------------------ |:----------------:|:--------------:|:---------------:|:----------:|:-----------:|
| <span class="text-nowrap"><i class="fa fa-leaf fa-fw"></i> 😏</span>         |        ✔         |       ✔        |        ✔        |     ✔      |      ✔      |
| <span class="text-nowrap"><i class="fa fa-pencil fa-fw"></i> 😍</span>     |        ✔         |       ✔        |        ✔        |     ✔      |      ✖      |
| <span class="text-nowrap"><i class="fa fa-umbrella fa-fw"></i> 🤔</span>     |        ✔         |       ✔        |        ✔        |     ✖      |      ✖      |
| <span class="text-nowrap"><i class="fa fa-lock fa-fw"></i> 😨</span>         |        ✔         |       ✔        |        ✖        |     ✔      |      ✖      |
| <span class="text-nowrap"><i class="fa fa-fire fa-fw"></i> 😱</span>  |        ✔         |       ✔        |        ✖        |     ✖      |      ✖      |
| <span class="text-nowrap"><i class="fa fa-hand-stop-o fa-fw"></i> 🤢</span> |        ✔         |       ✖        |        ✖        |     ✖      |      ✖      |

**Evaluation after the Project**

:::success
Yes :tada:
:::


