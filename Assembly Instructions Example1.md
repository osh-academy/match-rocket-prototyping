# Title: How to Build a Rocketship during Corona

## Example Number: 1

### OVERVIEW:

These are a set of instructions on how to build a miniature rocketship using easily available materials. Follow the instructions and pictures below

--- 

### SAFETY:
* Don't do this inside - your Hausmeister will hate you!
* Don't point your rocket at people!
* The rocket doesn't fly as far on windy days
* Be safe - hold it away from yourself.
---

### MATERIALS:
* Match Sticks (one per rocket)
* Aluminium Foil
* Rocket Holder (for example... empty ballpoint pen ink cartridge)
___

### TOOLS:
* Pliers
* Scissors
* Ruler
* Candle or lighter
---

### METHODOLOGY:
1. Let's prep: Collect your materials and tools [see what you need above]

<img src="images/Ex1_Materials-Tools.png" width="400" height="400"/>


2.  Cut out the template as per the figure below. Follow the dimensions:

<img src="images/Ex1_Measurements.png" width="400"/>


3. Cut the match heads off the match sticks

<img src="images/Ex1_Matchsticks.png" width="400" height="400"/>


4. Lay the match heads onto the flat piece of aluminium foil at long breadth end. 

<img src="images/Ex1__requried_materials_to_build_rocket.png" width="400" height="400"/>


5. Roll, roll, roll: Roll the foil around the pen and match head.  Roll the tube as tightly and neatly as possible. Make sure not to crush the matchhead when you're rolling!

6. Crimp the tip with your pliers. Make sure that when you're crimping it is completely sealed

<img src="images/Ex_1_complete_assembled_rocket.png" width="400" height="400"/>


7. Hold the pen bit of the rocket, hold the candle (or lighter) to the top of the rocket where the matchstick head is sitting. Check out the picture below!

<img src="images/Ex1_final_rocket_firing.png" width="400" height="400"/>


